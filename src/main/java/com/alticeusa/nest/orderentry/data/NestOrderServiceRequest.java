package com.alticeusa.nest.orderentry.data;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "nestOrder")
public class NestOrderServiceRequest {

	private String sourceApp = "";
	private String footprint = "";
	private String ctype;
	private String accountNumber = "";
	private String linkOrderId = "";
	private String firstName;
	private String lastName;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country = "US";
	private String zipCode;
	private String email;
	private String phone;
	private String signatureRequired = "N";
	private ArrayList<Equipment> equipments;
	private ArrayList<Subscription> subscriptions;
	private String shippingCost = "0.00";
	private String orderCost;
	private String orderTaxes;
	private String description;
	private String franchiseArea;
	
	@XmlElement (name="franchiseArea", required = true)
	public String getFranchiseArea() {
		return franchiseArea;
	}

	public void setFranchisearea(String franchiseArea) {
		this.franchiseArea = franchiseArea;
	}

	@XmlElement (name="sourceApp", required = true)
	public String getSourceApp() {
		return sourceApp;
	}

	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}

	@XmlElement (name="footprint", required = true)
	public String getFootprint() {
		return footprint;
	}

	public void setFootprint(String footprint) {
		this.footprint = footprint;
	}

	public String getCtype() {
		return ctype;
	}

	@XmlElement (name="ctype", required = true)
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	@XmlElement (name="accountNumber", required = true)
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@XmlElement (name="linkOrderId")
	public String getLinkOrderId() {
		return linkOrderId;
	}

	public void setLinkOrderId(String linkOrderId) {
		this.linkOrderId = linkOrderId;
	}

	@XmlElement (name="firstName", required = true)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@XmlElement (name="lastName", required = true)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@XmlElement (name="address1", required = true)
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	@XmlElement (name="address2")
	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	@XmlElement (name="city", required = true)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@XmlElement (name="state", required = true)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@XmlElement (name="country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@XmlElement (name="zipCode", required = true)
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@XmlElement (name="email", required = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@XmlElement (name="phone", required = true)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@XmlElement (name="signatureRequired")
	public String getSignatureRequired() {
		return signatureRequired;
	}

	public void setSignatureRequired(String signatureRequired) {
		this.signatureRequired = signatureRequired;
	}

	@XmlElementWrapper(name="equipments")
	@XmlElement (name="equipment")
	public ArrayList<Equipment> getEquipments() {
		return equipments;
	}

	public void setEquipments(ArrayList<Equipment> equipments) {
		this.equipments = equipments;
	}

	@XmlElementWrapper(name="subscriptions")
	@XmlElement (name="subscription")
	public ArrayList<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(ArrayList<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	@XmlElement (name="shippingCost")
	public String getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	@XmlElement (name="orderCost", required = true)
	public String getOrderCost() {
		return orderCost;
	}

	public void setOrderCost(String orderCost) {
		this.orderCost = orderCost;
	}

	@XmlElement (name="orderTaxes", required = true)
	public String getOrderTaxes() {
		return orderTaxes;
	}

	public void setOrderTaxes(String orderTaxes) {
		this.orderTaxes = orderTaxes;
	}

	@XmlElement (name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
