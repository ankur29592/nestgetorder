package com.alticeusa.nest.orderentry.data;

public class NestOrderServiceDaoResponse {

	NestOrderEquipmentPojo nestOrderEquipmentPojo;
	NestOrderSubscriptionList nestOrderSubscriptionList;
	NestOrderServiceResponse nestOrderServiceResponse;
	
	public NestOrderServiceDaoResponse() {
	}
	
	public NestOrderServiceDaoResponse(NestOrderEquipmentPojo nestOrderEquipmentPojo,NestOrderSubscriptionList nestOrderSubscriptionList,NestOrderServiceResponse nestOrderServiceResponse) {
		this.nestOrderEquipmentPojo = nestOrderEquipmentPojo;
		this.nestOrderSubscriptionList = nestOrderSubscriptionList;
		this.nestOrderServiceResponse = nestOrderServiceResponse;
	}
	
	public NestOrderServiceResponse getNestOrderEntryResponse() {
		return nestOrderServiceResponse;
	}
	public void setNestOrderEntryResponse(NestOrderServiceResponse nestOrderServiceResponse) {
		this.nestOrderServiceResponse = nestOrderServiceResponse;
	}
	public NestOrderEquipmentPojo getNestOrderEquipmentPojo() {
		return nestOrderEquipmentPojo;
	}
	public void setNestOrderEquipmentPojo(NestOrderEquipmentPojo nestOrderEquipmentPojo) {
		this.nestOrderEquipmentPojo = nestOrderEquipmentPojo;
	}
	public NestOrderSubscriptionList getNestOrderSubscriptionList() {
		return nestOrderSubscriptionList;
	}
	public void setNestOrderSubscriptionList(NestOrderSubscriptionList nestOrderSubscriptionList) {
		this.nestOrderSubscriptionList = nestOrderSubscriptionList;
	}
	
}
