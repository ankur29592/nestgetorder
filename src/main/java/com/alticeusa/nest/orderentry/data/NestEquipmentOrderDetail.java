package com.alticeusa.nest.orderentry.data;

import org.codehaus.jackson.annotate.JsonProperty;

public class NestEquipmentOrderDetail {

	
	private String firstName;
	
	private String lastName;
	
	private String address1;
	
	private String address2;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String POSTAL_CODE;
	
	private String email;
	
	private String phone;
	
	private String LANGUAGE_PREFRENCE = "en-us";
	
	private String ORDER_NUMBER;
	
	private String ORDER_DATE;
	
	private String SKU;
	
	private String QUANTITY;
	
	private String signatureRequired = "N";
	
	private String OPTION1;
	
	
	public String getFirstName() {
		return firstName;
	}
	@JsonProperty("FIRST_NAME")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	@JsonProperty("LAST_NAME")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress1() {
		return address1;
	}
	@JsonProperty("ADDRESS1")
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	@JsonProperty("ADDRESS2")
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	@JsonProperty("CITY")
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	@JsonProperty("STATE")
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	@JsonProperty("COUNTRY")
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPOSTAL_CODE() {
		return POSTAL_CODE;
	}
	@JsonProperty("POSTAL_CODE")
	public void setPOSTAL_CODE(String pOSTAL_CODE) {
		POSTAL_CODE = pOSTAL_CODE;
	}
	public String getEmail() {
		return email;
	}
	@JsonProperty("EMAIL")
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	@JsonProperty("PHONE")
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLANGUAGE_PREFRENCE() {
		return LANGUAGE_PREFRENCE;
	}
	@JsonProperty("LANGUAGE_PREFRENCE")
	public void setLANGUAGE_PREFRENCE(String lANGUAGE_PREFRENCE) {
		LANGUAGE_PREFRENCE = lANGUAGE_PREFRENCE;
	}
	public String getORDER_NUMBER() {
		return ORDER_NUMBER;
	}
	@JsonProperty("ORDER_NUMBER")
	public void setORDER_NUMBER(String oRDER_NUMBER) {
		ORDER_NUMBER = oRDER_NUMBER;
	}
	public String getORDER_DATE() {
		return ORDER_DATE;
	}
	@JsonProperty("ORDER_DATE")
	public void setORDER_DATE(String oRDER_DATE) {
		ORDER_DATE = oRDER_DATE;
	}
	public String getSKU() {
		return SKU;
	}
	@JsonProperty("SKU")
	public void setSKU(String sKU) {
		SKU = sKU;
	}
	public String getQUANTITY() {
		return QUANTITY;
	}
	@JsonProperty("QUANTITY")
	public void setQUANTITY(String qUANTITY) {
		QUANTITY = qUANTITY;
	}
	public String getSignatureRequired() {
		return signatureRequired;
	}
	@JsonProperty("SIGNATURE_REQUIRED")
	public void setSignatureRequired(String signatureRequired) {
		this.signatureRequired = signatureRequired;
	}
	
	public String getOPTION1() {
		return OPTION1;
	}
	@JsonProperty("OPTION1")
	public void setOPTION1(String oPTION1) {
		OPTION1 = oPTION1;
	}
	
}
