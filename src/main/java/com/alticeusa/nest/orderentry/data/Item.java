package com.alticeusa.nest.orderentry.data;

import org.codehaus.jackson.annotate.JsonProperty;

public class Item {

	@JsonProperty("ORIGINAL_INDEX")
    private Integer originalIndex;
    @JsonProperty("ORDER_NUMBER")
    private String orderNumber;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("COMMENTS")
    private String comments;
    @JsonProperty("VALIDATION_ERRORS")
    private String validationErrors;
    
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getValidationErrors() {
		return validationErrors;
	}
	public void setValidationErrors(String validationErrors) {
		this.validationErrors = validationErrors;
	}
	public Integer getOriginalIndex() {
		return originalIndex;
	}
	public void setOriginalIndex(Integer originalIndex) {
		this.originalIndex = originalIndex;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}

