package com.alticeusa.nest.orderentry.data;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

public class NestSubscriptionError {

	private String type;
	private ArrayList<NestSubscriptionFieldViolation> nestSubscriptionFieldViolations;
	@JsonProperty("reason")
    private String reason;
    @JsonProperty("domain")
    private String domain;
    @JsonProperty("message")
    private String message;
    
    
    
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ArrayList<NestSubscriptionFieldViolation> getFieldViolations() {
		return nestSubscriptionFieldViolations;
	}
	public void setFieldViolations(ArrayList<NestSubscriptionFieldViolation> nestSubscriptionFieldViolations) {
		this.nestSubscriptionFieldViolations = nestSubscriptionFieldViolations;
	}
}
