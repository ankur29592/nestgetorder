package com.alticeusa.nest.orderentry.utils;

public class Constant {

	public static NestOrderServiceProperties prop = NestOrderServiceProperties.getInstance();
	public static final String ERR_400 = "400";
	public static final String ERR_400_message = "Required Field/s are missing ";
	public static final String ERR_500 = "500";
	public static final String ERR_500_message = "Internal server error, due to an unexpected condition";
	public static final String SUCCESS = "TRUE";
	public static final String FAIL = "FALSE";
	public static final String ERR_404 = "404";
	public static final String ERROR = "ERROR";
	public static final String NEST_DEFAULT_ERROR_STATUS = null;
	public static final String EQUIP_FILE_NAME = "EQUIPMENT";
	public static final String SUBSCRIB_FILE_NAME = "SUBSCRIPTION";
	public static final String LOG_FILE_PATH = prop.getValue("LOG_FILE_PATH");
	//public static final String LOG_FILE_PATH="C:\\Nest_Logs\\";
	public static final String ORDERED_STATUS = "ORDERED";
	public static final String ECOMMERCE="ECOMMERCE";
	public static final long WAIT_TIME=60000;
	
	public static final String NEST_ORDERENTRY_SEND_URL = prop.getValue("NEST_ORDERENTRY_SEND_URL");
	public static final String CONNECTION_FACTORY = prop.getValue("CONNECTION_FACTORY");
	public static final String NEST_ORDERENTRY_SEND_CONTEXT_FACTORY = prop.getValue("NEST_ORDERENTRY_SEND_CONTEXT_FACTORY");
	public static final String NEST_ORDER_EQUIP_QUEUE_NAME = prop.getValue("NEST_ORDER_EQUIP_QUEUE");
	public static final String NEST_ORDER_SUBSCRB_QUEUE_NAME = prop.getValue("NEST_ORDER_SUBSCRB_QUEUE");
	public static final int NEST_ERROR_THRESHOLD_VALUE = Integer.parseInt(prop.getValue("NEST_ERROR_THRESHOLD_VALUE"));
	
	public static final String DB_DRIVER = prop.getValue("DB_DRIVER");
	public static final String DB_CONNECTION = prop.getValue("DB_CONNECTION");
	public static final String DB_USER = prop.getValue("DB_USER");
	public static final String DB_PASSWORD = prop.getValue("DB_PASSWORD");
	
	public static final String API_VERSION = "V1";
	public static final String API_NAME = "nestOrderService";
	//public static boolean PRETTY_PRINT = StringFormatter.toBoolean(prop.getValue("logJSONPrettyPrint"));
	
	public static final String CEMS_EMAIL_SERVER = prop.getValue("CEMS_EMAIL_SERVER");
	public static final String CEMS_URL= prop.getValue("CEMS_URL");
	public static final String USER_NAME= prop.getValue("USER_NAME");
	public static final String PASSWORD= prop.getValue("PASSWORD");
	public static final String CEMS_TEMPLATE= prop.getValue("CEMS_TEMPLATE");
	
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_KEY= "Datetime";
	public static final String METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY = "Thread";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY = "contextName";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY = "SubContextName";
	public static final String METRICS_LOGGING_REPEATER_LOG_ENTRY_TYPR = "LogEntryType";
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_FORMAT = "YYYY-MM-dd'T'HH:mm:ss.SSS";
	
	public static final String METRICS_LOGGING_REQUEST_SOURCEAPP = "SourceApp";
	public static final String METRICS_LOGGING_REQUEST_FOOTPRINT = "FootPrint";
	public static final String METRICS_LOGGING_REQUEST_CTYPE = "CType";
	public static final String METRICS_LOGGING_REQUEST_ACCOUNTNUMBER = "AccountNumber";
	public static final String METRICS_LOGGING_REQUEST_LINKORDERID = "LinkOrderId";
	public static final String METRICS_LOGGING_REQUEST_FIRSTNAME = "FirstName";
	public static final String METRICS_LOGGING_REQUEST_LASTNAME = "LastName";
	public static final String METRICS_LOGGING_REQUEST_ADDRESS1 = "Address1";
	public static final String METRICS_LOGGING_REQUEST_ADDRESS2 = "Address2";
	public static final String METRICS_LOGGING_REQUEST_CITY = "City";
	public static final String METRICS_LOGGING_REQUEST_STATE = "State";
	public static final String METRICS_LOGGING_REQUEST_COUNTRY = "Country";
	public static final String METRICS_LOGGING_REQUEST_EMAIL = "Email";
	public static final String METRICS_LOGGING_REQUEST_PHONE = "Phone";
	public static final String METRICS_LOGGING_REQUEST_SIGNATUREREQUIRED = "SignatureRequired";
	public static final String METRICS_LOGGING_REQUEST_RATECODE = "RateCode";
	public static final String METRICS_LOGGING_REQUEST_QUANTITY = "Quantity";
	public static final String METRICS_LOGGING_REQUEST_COST = "Cost";
	public static final String METRICS_LOGGING_REQUEST_SERIALNUMBER = "SerialNumber";
	public static final String METRICS_LOGGING_REQUEST_TAX = "Tax";
	public static final String METRICS_LOGGING_REQUEST_EQUIPMENTS = "Equipments";
	
	public static final String METRICS_LOGGING_REQUEST_SUBS_DISPLAYNAME = "DisplayName";
	public static final String METRICS_LOGGING_REQUEST_SUBS_RATECODE = "RateCode";
	public static final String METRICS_LOGGING_REQUEST_SUBS_QUANTITY = "Quantity";
	
	public static final String METRICS_LOGGING_REQUEST_SUBSCRIPTIONS = "Subscriptions";
	
	
	public static final String METRICS_LOGGING_REQUEST_ORDERCOST = "OrderCost";
	public static final String METRICS_LOGGING_REQUEST_ORDERTAXES = "OrderTaxes";
	public static final String METRICS_LOGGING_REQUEST_DESCRIPTION = "Description";
	public static final String METRICS_LOGGING_REQUEST_ZIPCODE = "ZipCode";
	
	
	public static final String METRICS_LOGGING_COMMMON_SUCCESS_KEY = "Success";
	public static final String METRICS_LOGGING_COMMON_ERROR_CODE_KEY = "ErrorCode";
	public static final String METRICS_LOGGING_COMMON_ERROR_MESSAGE_KEY = "ErrorMessage";
	
	
	public static final String METRICS_LOGGING_COMMON_CLASS_NAME_KEY = "ClassName";
	public static final String METRICS_LOGGING_CLASS_NAME_VALUE = "com.alticeusa.nest.orderentry";
	public static final String METRICS_LOGGING_COMMON_METHOD_NAME_KEY = "MethodName";
	public static final String METRICS_LOGGING_METHOD_NAME_VALUE = "NestOrderService";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE = "NEST-Provisioning";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE = "NestOrderService";
	public static final String METRICS_LOGGING_COMMON_METHOD_NAME_VALUE = "NestOrderService";
	public static final String EMPTY_JSON= "{}";
	public static final String SPACE=" ";
	public static final String DASH="-";
	
	public static final String METRICS_LOGGING_COMMON_ERROR_CODE="ErrorCode";
	public static final String METRICS_LOGGING_COMMON_ERROR_MESSAGE= "ErrorMessage";
	public static final String METRICS_LOGGING_COMMON_SOURCEAPP = "sourceApp";
	public static final String METRICS_LOGGING_COMMON_FOOTPRINT = "footprint";
	public static final String METRICS_LOGGING_COMMON_ORDERNUMBER = "orderNumber";
	public static final String METRICS_LOGGING_COMMON_ACCOUNTNUMBER = "accountNumber";
	public static final String METRICS_LOGGING_COMMON_LINKORDERID = "linkOrderId";
	public static final String METRICS_LOGGING_COMMON_SUCCESS = "success";
	public static final String METRICS_LOGGING_COMMON_ERRORCODE = "errorCode";
	public static final String METRICS_LOGGING_COMMON_ERRORMESSAGE = "errorMessage";
	public static boolean PRETTY_PRINT = StringFormatter.toBoolean("true");
	public static final String NEST_LOGO="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/Nest-Shipping-Email_01.jpg";
	public static final String NEST_HEADER_BAR="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/Nest-Shipping-Email_02.jpg";
    public static final String OPTIMUM_LOGO="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/Nest-Shipping-Email_03.jpg";
    public static final String MAIL_IMAGE="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/Nest-Order-Confirmation-Email_01.jpg";
    public static final String SPACER="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/spacer.gif";
    public static final String OPTIMUM_BRAND_ALTICE_LOGO="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/Nest-Shipping-Email_13.jpg";
    public static final String HELLOTECH_LOGO="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/Nest-Order-Confirmation-Email_04.jpg";
    public static final String CART_IMAGE="http://epidm.edgesuite.net/CMS/Coding/CableVision/2017/11-Nov/01344992/Nest-Order-Confirmation-Email_02.jpg";
    public static final String HELLOTECH_URL="https://www.hellotech.com/optimumnest";
    public static final String OPTIMUM_LOGO_URL=prop.getValue("OPTIMUM_LOGO_URL");
    public static final String CUSTOMER_PRIVACY_POLICY=prop.getValue("CUSTOMER_PRIVACY_POLICY");
    public static final String SUBSCRIPTION_ACTIVATION_URL=prop.getValue("SUBSCRIPTION_ACTIVATION_URL");
}
