package com.alticeusa.nest.orderentry.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

	private static PropertiesReader singleton = null;
    private Properties props;
    
    private PropertiesReader()
    {
    	 InputStream is = PropertiesReader.class.getClassLoader().getResourceAsStream("nestOrderService.properties");
         props = new Properties();
         try
         {
             props.load(is);
         }//try
         catch (IOException e)
         {
             //logger.error("Unable to Load Property File: " + e.getMessage(),e);
         }//catch
    }//PropertiesReader
    
    public static PropertiesReader getInstance()
    {
    	if (singleton == null)
    		singleton = new PropertiesReader();
    	return singleton;
    }//getInstance 
   
	/**
	 * @return
	 */
	public Properties getParams()
    {
        return props ;
    }//getParams 
	
	/**
	 * @param key
	 * @return
	 */
	public String getValue(String key)
	{
		return props.getProperty(key);
	}//getValue
	
	/*public void printProperties()
	{
		if (null != props)
		{
			for (Enumeration<?> en = props.keys(); en.hasMoreElements();)
			{
				String key = (String)en.nextElement();
				//logger.info(StringFormatter.formatString("key = {0}, value = {1}", key, props.get(key)));
			}//for
		}//if
	}*/
}
