package com.alticeusa.nest.orderentry.BO;

import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.dao.NestOrderServiceDao;
import com.alticeusa.nest.orderentry.data.Equipment;
import com.alticeusa.nest.orderentry.data.NestOrderServiceDaoResponse;
import com.alticeusa.nest.orderentry.data.NestOrderServiceRequest;
import com.alticeusa.nest.orderentry.data.NestOrderServiceResponse;
import com.alticeusa.nest.orderentry.data.NestOrderSubscriptionPojo;
import com.alticeusa.nest.orderentry.data.Subscription;
import com.alticeusa.nest.orderentry.metrics.logging.LogFormatter;
import com.alticeusa.nest.orderentry.queue.NestOrderEquipmentSender;
import com.alticeusa.nest.orderentry.queue.NestOrderSubscriptionSender;
import com.alticeusa.nest.orderentry.utils.Constant;
import com.alticeusa.nest.orderentry.utils.StringFormatter;
import com.alticeusa.nest.orderentry.utils.Utils;


public class NestOrderServiceBO {

	private static final Logger logger = LogManager.getLogger(NestOrderServiceBO.class);
	/**
	 * 
	 * @param nestOrderServiceRequest
	 * @return NestOrderEntryResponse
	 */
	public NestOrderServiceResponse orderEntry(NestOrderServiceRequest nestOrderServiceRequest){
		NestOrderServiceResponse nestOrderServiceResponse = new NestOrderServiceResponse();
		NestOrderServiceDaoResponse nestOrderServiceDaoResponse = null;
		int sendRetry = Constant.NEST_ERROR_THRESHOLD_VALUE;
		String errorStackTrace=null;
		long start = System.currentTimeMillis();
		
		try {
			Utils.performMetricsLogging(nestOrderServiceRequest,nestOrderServiceResponse);
			Utils.populateResponseWithValuesFromRequest(nestOrderServiceRequest, nestOrderServiceResponse);
			Utils.performNecessaryLogging(nestOrderServiceRequest,nestOrderServiceResponse,errorStackTrace,start);
			logger.info(">>> orderEntry() ::: request = {}", LogFormatter.produceJSON(nestOrderServiceRequest, Constant.PRETTY_PRINT));
			//NestOrderServiceRequest requestToDB=createRequestToSendToDatabase(nestOrderServiceRequest);
			if(!"".equals(Utils.checkValidRequests(nestOrderServiceRequest)) ) {
				
				logger.debug("Check valid request failed.");
				String errMsg = Utils.checkValidRequests(nestOrderServiceRequest);
				nestOrderServiceResponse = createResponse(nestOrderServiceRequest,nestOrderServiceResponse,Constant.FAIL);
				nestOrderServiceResponse = Utils.sendError(nestOrderServiceResponse,Constant.ERR_400,errMsg.trim());
				Utils.performNecessaryLogging(nestOrderServiceRequest,nestOrderServiceResponse,errorStackTrace,start);
				Utils.writeLogForError(nestOrderServiceRequest);
				return nestOrderServiceResponse;
				
			} else if(!"".equals(Utils.validateRequest(nestOrderServiceRequest))) {
				
				logger.debug("Check validate request failed.");
				String errMsg = Utils.validateRequest(nestOrderServiceRequest);
				nestOrderServiceResponse = createResponse(nestOrderServiceRequest,nestOrderServiceResponse,Constant.FAIL);
				nestOrderServiceResponse = Utils.sendError(nestOrderServiceResponse,Constant.ERR_400,errMsg.trim());
				Utils.performNecessaryLogging(nestOrderServiceRequest,nestOrderServiceResponse,errorStackTrace,start);
				Utils.writeLogForError(nestOrderServiceRequest);
				return nestOrderServiceResponse;
				
			}else{
				if(nestOrderServiceRequest.getEquipments()==null) {
					nestOrderServiceRequest.setEquipments(new ArrayList<Equipment>());
				}
				
				if(nestOrderServiceRequest.getSubscriptions()==null) {
					nestOrderServiceRequest.setSubscriptions(new ArrayList<Subscription>());
				}
				if(StringFormatter.nullOrBlank(nestOrderServiceRequest.getLinkOrderId())) {
					nestOrderServiceRequest.setLinkOrderId("");
				}
				if(StringFormatter.nullOrBlank(nestOrderServiceRequest.getAddress2())) {
					nestOrderServiceRequest.setAddress2("");
				}
				if(StringFormatter.nullOrBlank(nestOrderServiceRequest.getDescription())) {
					nestOrderServiceRequest.setDescription("");
				}
				for(Equipment equipment:nestOrderServiceRequest.getEquipments()) 
				{
					if(StringFormatter.nullOrBlank(equipment.getCost())) {
						equipment.setCost("0.00");
					}
					if(StringFormatter.nullOrBlank(equipment.getTax())) {
						equipment.setTax("0.00");
					}
					if(StringFormatter.nullOrBlank(equipment.getSerialNumber()))
					{
						equipment.setSerialNumber("");
					}
					if(StringFormatter.nullOrBlank(equipment.getQuantity()))
					{
						equipment.setQuantity("1");
					}
				}
				for(Subscription subscription:nestOrderServiceRequest.getSubscriptions()) {
					if(StringFormatter.nullOrBlank(subscription.getDisplayName())) {
						subscription.setDisplayName("");
					}
					if(StringFormatter.nullOrBlank(subscription.getQuantity()))
					{
						subscription.setQuantity("1");
					}
				}
				if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getCountry())) {
					nestOrderServiceRequest.setCountry("US");
				}
				if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getSignatureRequired())) {
					nestOrderServiceRequest.setSignatureRequired("N");
				}
				if((nestOrderServiceRequest.getSourceApp()).equalsIgnoreCase(Constant.ECOMMERCE))
				{
					logger.info("Source is ECOMMERCE");
					ArrayList<Equipment> eqs=new ArrayList<Equipment>();
					for(Equipment eq:nestOrderServiceRequest.getEquipments())
					{
						if(Integer.parseInt(eq.getQuantity())>=1)
						{
							logger.info("Cost: "+eq.getCost());
							logger.info("Tax: "+eq.getTax());
							
							double cost=Double.parseDouble(eq.getCost());
							double tax=Double.parseDouble(eq.getTax());
							logger.info("double cost: "+cost);
							logger.info("double tax: "+tax);
							double quantity=Double.parseDouble(eq.getQuantity());		
							double perEquipCost=cost/quantity;
							double perCost=(double)Math.round(perEquipCost * 100)/100;
							double perEquipTax=tax/quantity;
							double perTax=(double)Math.round(perEquipTax * 100)/100;
							logger.info("perCost: "+perCost);
							logger.info("perTax: "+perTax);
							
							for(int i=0;i<Integer.parseInt(eq.getQuantity());i++)
							{
								Equipment eqDB=new Equipment();
								
								eqDB.setCost(Double.toString(perCost));
								eqDB.setQuantity("1");
								eqDB.setRateCode(eq.getRateCode());
								if(eq.getSerialNumber()!=null)
								{
								eqDB.setSerialNumber(eq.getSerialNumber());
								}
								else
								{
									eqDB.setSerialNumber("");
								}
								eqDB.setTax(Double.toString(perTax));
								eqs.add(eqDB);
							}
						}
					}
					nestOrderServiceRequest.setEquipments(eqs);
					ArrayList<Subscription> subs=new ArrayList<Subscription>();
					for(Subscription sub:nestOrderServiceRequest.getSubscriptions())
					{
						if(Integer.parseInt(sub.getQuantity())>=1)
						{
							for(int i=0;i<Integer.parseInt(sub.getQuantity());i++)
							{
								Subscription subDB=new Subscription();
								subDB.setDisplayName(sub.getDisplayName());
								subDB.setQuantity("1");
								subDB.setRateCode(sub.getRateCode());
								subs.add(subDB);
							}
						}
					}
					nestOrderServiceRequest.setSubscriptions(subs);
				}
				else
				{
				for(Equipment equipment:nestOrderServiceRequest.getEquipments()) {
					if(equipment.getSerialNumber()==null) {
						equipment.setSerialNumber("");
					}
					if(Integer.parseInt(equipment.getQuantity())>1) {
						equipment.setQuantity("1");
					}
				}
				for(Subscription subs:nestOrderServiceRequest.getSubscriptions()) {
					if(Integer.parseInt(subs.getQuantity())!=1) {
						subs.setQuantity("1");
					}
				}
				}
				logger.debug("Convert request into Xml.");
				String orderInfoXml = Utils.convertJavaToXml(nestOrderServiceRequest);
				NestOrderServiceDao nestOrderServiceDao = new NestOrderServiceDao();
				//System.out.println(orderInfoXml);
				nestOrderServiceDaoResponse = nestOrderServiceDao.createOrder(nestOrderServiceRequest, orderInfoXml,nestOrderServiceResponse);
				
				if( StringFormatter.nullOrBlank(nestOrderServiceDaoResponse.getNestOrderEntryResponse().getErrorCode())&& StringFormatter.nullOrBlank(nestOrderServiceDaoResponse.getNestOrderEntryResponse().getErrorMessage())) {
					logger.debug("Creating Sucess Response.");
					nestOrderServiceResponse = createResponse(nestOrderServiceRequest,nestOrderServiceDaoResponse.getNestOrderEntryResponse(),Constant.SUCCESS);
				} else {
					logger.debug("Creating Failure Response.");
					nestOrderServiceResponse = createResponse(nestOrderServiceRequest,nestOrderServiceDaoResponse.getNestOrderEntryResponse(),Constant.FAIL);
					Utils.writeLogForError(nestOrderServiceRequest);
				}
			}
		} catch (JAXBException e) {
			logger.error("Exception occured while converting Java To Xml:", e);
				nestOrderServiceResponse = createResponse(nestOrderServiceRequest,nestOrderServiceResponse,Constant.FAIL);
				nestOrderServiceResponse = Utils.sendError(nestOrderServiceResponse,Constant.ERR_500,Constant.ERR_500_message);
				Utils.writeLogForError(nestOrderServiceRequest);
				return nestOrderServiceResponse;
		} catch (Exception e) {
			logger.error("Exception occured while calling create order:", e);
				nestOrderServiceResponse = createResponse(nestOrderServiceRequest,nestOrderServiceResponse,Constant.FAIL);
				nestOrderServiceResponse = Utils.sendError(nestOrderServiceResponse,Constant.ERR_500,Constant.ERR_500_message);
				Utils.writeLogForError(nestOrderServiceRequest);
				return nestOrderServiceResponse;
		}
		try {
			if(nestOrderServiceDaoResponse.getNestOrderEquipmentPojo()!=null && !nestOrderServiceDaoResponse.getNestOrderEquipmentPojo().getOrders().isEmpty()) {
				String nestOrderEquipmentJson = Utils.convertJavaToJson(nestOrderServiceDaoResponse.getNestOrderEquipmentPojo());
				NestOrderEquipmentSender nestOrderEquipmentSender = NestOrderEquipmentSender.getInstance();
				logger.info(">>> sendMsgToQueue() ::: EquipmentQueuemessage = {}", LogFormatter.produceJSON(nestOrderServiceDaoResponse.getNestOrderEquipmentPojo(), Constant.PRETTY_PRINT));
				nestOrderEquipmentSender.sendMsgToQueue(nestOrderEquipmentJson,nestOrderServiceDaoResponse.getNestOrderEquipmentPojo().getOrderId(),sendRetry);
			}
				
			if(nestOrderServiceDaoResponse.getNestOrderSubscriptionList()!=null && !nestOrderServiceDaoResponse.getNestOrderSubscriptionList().getNestOrderSubscriptionPojoList().isEmpty()) {
				for(NestOrderSubscriptionPojo subPojo:nestOrderServiceDaoResponse.getNestOrderSubscriptionList().getNestOrderSubscriptionPojoList()){
					String nestOrderSubscriptionJson = Utils.convertJavaToJson(subPojo);
					NestOrderSubscriptionSender nestOrderSubscriptionSender = NestOrderSubscriptionSender.getInstance();
					logger.info(">>> sendMsgToSubscrbQueue() ::: SubscriptionQueuemessage = {}", LogFormatter.produceJSON(subPojo, Constant.PRETTY_PRINT));
					nestOrderSubscriptionSender.sendMsgToSubscrbQueue(nestOrderSubscriptionJson,subPojo.getPartnerCustomerId(),subPojo.getPartnerSubscriptionId(),sendRetry);
					
				}
			}
		} catch(Exception e) {
			logger.error("Exception occured while publishing the message in to queue:", e);
				System.out.println("write log file logic");
		}
		logger.info("send resoponse to Ecommerce.");
		Utils.performNecessaryLogging(nestOrderServiceRequest,nestOrderServiceResponse,errorStackTrace,start);
		//emailSender.send();
		return nestOrderServiceResponse;
	}
	
	/**
	 * 
	 * @param nestOrderServiceRequest
	 * @param nestOrderServiceResponse
	 * @param successStatus
	 * @return NestOrderEntryResponse
	 */
	private NestOrderServiceResponse createResponse(NestOrderServiceRequest nestOrderServiceRequest , NestOrderServiceResponse nestOrderServiceResponse,String successStatus) {
		nestOrderServiceResponse.setSourceApp(nestOrderServiceRequest.getSourceApp());
		nestOrderServiceResponse.setFootprint(nestOrderServiceRequest.getFootprint());
		nestOrderServiceResponse.setLinkOrderId(nestOrderServiceRequest.getLinkOrderId());
		nestOrderServiceResponse.setAccountNumber(nestOrderServiceRequest.getAccountNumber());
		nestOrderServiceResponse.setSuccess(successStatus);
		return nestOrderServiceResponse;
	}
	

	
}
