package com.alticeusa.nest.orderentry.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



public abstract class BaseDAO 
{
	private static final String ENV_CONTEXT = "java:comp/env";
	//private static final Logger logger = LogManager.getLogger(BaseDAO.class);
	private boolean transFlag = false; // TransactionFlag

	/**
	 * This methods makes a look up for the datasource for DB connection.
	 * @return
	 * @throws DbException
	 */
	public Connection getConnection(String DATASOURCE) throws Exception 
	{
		DataSource ds = null;
		Context ctx = null;
		Connection abstractConnection = null;
		try 
		{
			ctx = new InitialContext();
			Context envContext  = (Context)ctx.lookup(ENV_CONTEXT);
			ds = (javax.sql.DataSource) envContext.lookup(DATASOURCE);
			abstractConnection = ds.getConnection();
		}// try
		catch (Exception e) 
		{
			throw e;
		}// catch
		finally
		{
		    close (ctx);
		}
		return abstractConnection;
	}// getConnection

	/**
	 * This method closes the <code>context</code>.
	 * @param context
	 */
	public void close (Context context)
	{
        try
        {
        	if(context != null)
            context.close();
        }//try
        catch (NamingException e)
        {
         // Do nothing in this case.
        }//catch
	}//close
	
	/**
	 * This method closes the <code>connection</code>,<code>statement</code>,
	 * <code>resultSet</code>
	 * @param connection
	 * @param statement
	 * @param resultSet
	 */
	public void close(Connection connection, Statement statement,ResultSet resultSet) 
	{
		try 
		{
			if (resultSet != null) 
			{
				resultSet.close();
				resultSet = null;
			}// if

			if (statement != null) 
			{
				statement.close();
				statement = null;
			}// if

			if (connection != null) 
			{
				connection.close();
				connection = null;
				
			}// if
		}// try
		catch (SQLException e) 
		{
			e.printStackTrace();
		}// catch
	}// close

	public void finalize(Connection connection, Statement statement, ResultSet resultSet) 
	{
		transFlag = false;
		close(connection, statement, resultSet);
	}// finalize

	public Connection startTransaction(String dataSource) throws Exception 
	{
	    if (!transFlag) 
	    {
	    	Connection abstractConnection = getConnection(dataSource);
	    	abstractConnection.setAutoCommit(false);
			transFlag = true;
			return abstractConnection;
		}// if
		return null;
	}// startTransaction
}//BaseDAO