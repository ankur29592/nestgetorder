package com.alticeusa.nest.orderentry.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NestOrderServiceConncectionDao extends BaseDAO {

	private static final Logger logger = LogManager.getLogger(NestOrderServiceConncectionDao.class);
	private final String NEST_DS = "jdbc/NEST_DS";
	public static NestOrderServiceConncectionDao instance = null;
	public static NestOrderServiceConncectionDao getInstance() throws Exception  {
		if (instance == null)
		{
			instance = new NestOrderServiceConncectionDao();
		}//if
		return instance;
	}
	
	public Connection getDBConnection() throws ClassNotFoundException, SQLException {
		Connection dbConnection = null;
		try {
			dbConnection = getConnection(NEST_DS);
		} catch (Exception e) {
			if(e.getMessage()!=null)
				logger.debug("Error creating connection from DataSource :"+NEST_DS+" Exception :"+e.getMessage());
			logger.debug("Error creating connection from DataSource :"+NEST_DS);
		}
		return dbConnection;
	}
}
