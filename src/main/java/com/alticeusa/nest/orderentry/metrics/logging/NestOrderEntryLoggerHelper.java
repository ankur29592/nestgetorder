package com.alticeusa.nest.orderentry.metrics.logging;

import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.data.Equipment;
import com.alticeusa.nest.orderentry.data.NestOrderServiceRequest;
import com.alticeusa.nest.orderentry.data.NestOrderServiceResponse;
import com.alticeusa.nest.orderentry.data.Subscription;
import com.alticeusa.nest.orderentry.utils.Constant;
import com.alticeusa.nest.orderentry.utils.StringFormatter;
import com.alticeusa.nest.orderentry.utils.Utils;

public class NestOrderEntryLoggerHelper {

	private static final Logger logger = LogManager.getLogger(NestOrderEntryLoggerHelper.class);
	private final NestOrderServiceRequest request;
	private final NestOrderServiceResponse response;
	private final String dateTimeStamp;
	
	public NestOrderEntryLoggerHelper(NestOrderServiceRequest request, NestOrderServiceResponse response) {
		this.request = request;
		this.response = response;
		this.dateTimeStamp= Utils.getCurrentTimeStamp(Constant.METRICS_LOGGING_COMMON_DATE_TIME_FORMAT);
	}
	
	@Override
	public String toString() {
		return getLogMetricsJSONString();
	}
	
	
	private String getLogMetricsJSONString(){
		String metricsJSON= Constant.EMPTY_JSON;
		try{
			LinkedHashMap<String, Object> metricsKeyValuePairs = new LinkedHashMap<String , Object>();
			addHeaderKeyValuePairs(metricsKeyValuePairs);
			addRequestKeyValuePairs(metricsKeyValuePairs);
			addResponseKeyValuePairs(metricsKeyValuePairs);			
			metricsJSON = MetricsLoggingUtils.buildJSONStringFromMap(metricsKeyValuePairs);
		}
		catch(Exception e){
			logger.info(" Exception in metrics logging " +e.getMessage());
		}
		 return metricsJSON;
	}  
	
	
public void addHeaderKeyValuePairs(LinkedHashMap<String, Object> metricsKeyValuePairs){
		
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_DATE_TIME_KEY,dateTimeStamp);
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY, Thread.currentThread().getName());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY, Constant.METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE);
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY, Constant.METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE);
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_CLASS_NAME_KEY,Constant.METRICS_LOGGING_CLASS_NAME_VALUE);
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_METHOD_NAME_KEY,Constant.METRICS_LOGGING_METHOD_NAME_VALUE);
		
	}
	
	
	public void addRequestKeyValuePairs(LinkedHashMap<String , Object> metricsKeyValuePairs){
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_SOURCEAPP, request.getSourceApp());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_FOOTPRINT, request.getFootprint());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_CTYPE, request.getCtype());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_ACCOUNTNUMBER, request.getAccountNumber());
		if(request.getLinkOrderId()!= null)
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_LINKORDERID, request.getLinkOrderId());
		
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_FIRSTNAME, request.getFirstName());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_LASTNAME, request.getLastName());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_ADDRESS1, request.getAddress1());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_ADDRESS2, request.getAddress2());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_CITY, request.getCity());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_STATE, request.getState());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_COUNTRY, request.getCountry());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_ZIPCODE, request.getZipCode());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_EMAIL, request.getEmail());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_PHONE, request.getPhone());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_SIGNATUREREQUIRED, request.getSignatureRequired());
		for(Equipment equipment : request.getEquipments()) {
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_RATECODE, equipment.getRateCode());
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_QUANTITY, equipment.getQuantity());
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_COST, equipment.getCost());
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_SERIALNUMBER, equipment.getSerialNumber());
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_TAX, equipment.getTax());
		}
		for(Subscription subscription : request.getSubscriptions()) {
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_SUBS_RATECODE, subscription.getRateCode());
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_SUBS_QUANTITY, subscription.getQuantity());
			metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_SUBS_DISPLAYNAME, subscription.getDisplayName());
		}
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_ORDERCOST, request.getOrderCost());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_ORDERTAXES, request.getOrderTaxes());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_REQUEST_DESCRIPTION, request.getDescription());
	}
	
	
	private void addResponseKeyValuePairs(LinkedHashMap<String, Object> metricsKeyValuePairs){
		if(areResponseFieldsPresent()){
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_SOURCEAPP,response.getSourceApp());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_FOOTPRINT,response.getFootprint());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_ORDERNUMBER,response.getOrderNumber());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_ACCOUNTNUMBER,response.getAccountNumber());
		if(response.getLinkOrderId()!=null)
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_LINKORDERID,response.getLinkOrderId());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_SUCCESS,response.getSuccess());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_ERROR_CODE,response.getErrorCode());
		metricsKeyValuePairs.put(Constant.METRICS_LOGGING_COMMON_ERROR_MESSAGE,response.getErrorMessage());
		}
	}
	
	private boolean areResponseFieldsPresent() {
	    if(StringFormatter.checkNullString(response.getSourceApp(),response.getFootprint(),response.getOrderNumber(),response.getAccountNumber(),response.getSuccess(),response.getErrorCode(),response.getErrorMessage()))
	    {
	    	return false;
	    } else {
	    	return true;
	    }
	}
}
