package com.alticeusa.nest.orderentry.cems;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.data.NestEquipmentOrderDetail;
import com.alticeusa.nest.orderentry.data.NestOrderServiceDaoResponse;
import com.alticeusa.nest.orderentry.data.NestOrderServiceRequest;
import com.alticeusa.nest.orderentry.helper.NestOrderSubscriptionRecieverSuccess; 

public class EmailSender {
	private static final Logger logger = LogManager.getLogger(EmailSender.class);

	EmailData emailData = new EmailData();
	
	CemsEmailHandler emailHandler = new CemsEmailHandler();
	
	public void send(ResultSet rs_equip, String email, int orderId) throws Exception{
	
		try{
			logger.debug("In the send email ");
			emailData.setRecepientAdd(email);
			emailData.setOrderNumber(orderId);
			
			
			emailHandler.sendEmail(emailData,rs_equip);
		}catch (Exception e) {
			logger.error("Exception occured in EmailSender send()",e);
			throw e;
		}
		}
		
}
