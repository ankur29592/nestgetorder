package com.alticeusa.nest.orderentry.cems;

import java.io.BufferedWriter;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.dao.NestOrderServiceDao;
import com.alticeusa.nest.orderentry.utils.Constant;

import oracle.jdbc.Const;

public class HTMLGenerator {
	private static final Logger logger = LogManager.getLogger(HTMLGenerator.class);
	BufferedWriter bw = null;
	public StringBuffer htmlGenerator(EmailData emailData,ResultSet rs_equip){
	StringBuffer htmlBuffer = new StringBuffer(emailData.getHtml());
	int[] orderCount = new int[2];
	NestOrderServiceDao nestOrderServiceDao = new NestOrderServiceDao();
	orderCount = nestOrderServiceDao.checkSubscriptions(emailData.getOrderNumber());
	int equipCount=orderCount[0];
	int subCount = orderCount[1];
	logger.info("Subscription Count in HTMLGenerator: "+subCount);
	logger.info("Equipment Count in HTMLGenerator: "+equipCount);
	
	try{
		if(equipCount!=0)
		{
			htmlBuffer.append("<tr><td valign=\"top\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 20px;color:#0084d6;line-height:24px;mso-line-height-rule:exactly;padding:6px 0px 0px 0px ; font-weight: 600; text-decoration: none;\" >Products: </td></tr>");
    while(rs_equip.next()){
	
		htmlBuffer.append("<tr><td align=\"center\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" style=\"border-bottom: 1px solid #e1e1e1;\"><tr><td align=\"left\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\"><tr><td width=\"191\" align=\"center\" valign=\"top\"  style=\"padding: 9px 0 0px 0;\"><img src=\"");
		htmlBuffer.append(rs_equip.getString("IMAGE_URL"));
		htmlBuffer.append("\" alt=\"");
		htmlBuffer.append(rs_equip.getString("PRODUCT_NAME"));
		htmlBuffer.append("\" width=\"");
		htmlBuffer.append(rs_equip.getInt("WIDTH"));
		htmlBuffer.append("\" height=\"");
		htmlBuffer.append(rs_equip.getInt("HEIGHT"));
		htmlBuffer.append("\" border=\"0\" style=\"display: block;\" /></td><td align=\"left\" valign=\"middle\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\"><tr><td  valign=\"middle\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 16px;color:#000000;line-height:20px;mso-line-height-rule:exactly;padding:0px 0px 0px 0px ; font-weight: 600; text-decoration: none\">");
		htmlBuffer.append(rs_equip.getString("PRODUCT_NAME"));
		htmlBuffer.append("</td></tr><tr><td  valign=\"middle\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 14px;color:#000000;line-height:20px;mso-line-height-rule:exactly;padding:0px 0px 0px 0px ; font-weight: 300; text-decoration: none\">Quantity: ");
		htmlBuffer.append(rs_equip.getString("QUANTITY"));
		htmlBuffer.append("</td></tr></table></td></tr></table></td></tr><tr><td align=\"left\" valign=\"top\"><img src=\""+Constant.SPACER+"\" alt=\"\" width=\"27\" height=\"16\" border=\"0\" style=\"display: block;\" /></td></tr></table></td></tr>");
		}
		}
		if(subCount!=0)
		{
			htmlBuffer.append("<tr><td valign=\"top\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 20px;color:#0084d6;line-height:24px;mso-line-height-rule:exactly;padding:31px 0px 0px 0px ; font-weight: 600; text-decoration: none;\" >Subscriptions: </td></tr><tr><td valign=\"top\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 16px;color:#000000;line-height:22px;mso-line-height-rule:exactly;padding:10px 0px 0px 0px ; font-weight: 300; text-decoration: none;\" >Nest Aware Basic/Extended - 10 day/30 day<br/><a target=\"_blank\" href=\""+Constant.SUBSCRIPTION_ACTIVATION_URL+"\" style=\"color: #0084d6; text-decoration: none;\"><span style=\"color: #0084d6; text-decoration: none;\">Click here for your activation code(s).</span></a> </td></tr><tr><td valign=\"top\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 16px;color:#000000;line-height:22px;mso-line-height-rule:exactly;padding:30px 0px 0px 0px ; font-weight: 300; text-decoration: none;\" >These subscription charge(s) will be added to your monthly Optimum bill. </td></tr>");
		}
		htmlBuffer.append("<tr><td align=\"left\" valign=\"top\" style=\"padding-top: 47px; padding-bottom: 29px;\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">");
		if(equipCount!=0)
		{
			htmlBuffer.append("<tr><td width=\"72\" align=\"left\" valign=\"middle\"  style=\"padding: 0px 0 0px 0;\"><img src=\""+Constant.MAIL_IMAGE+"\" alt=\"sms\" width=\"54\" height=\"38\" border=\"0\" style=\"display: block;\" /></td><td valign=\"top\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 16px;color:#000000;line-height:22px;mso-line-height-rule:exactly;padding:0px 20px 0px 0px ; font-weight: 300; text-decoration: none;\" >We'll send you an email with tracking information when your products ship.</td></tr>");
		}
	htmlBuffer.append("<tr><td width=\"72\" align=\"left\" valign=\"middle\"  style=\"padding: 18px 0 0px 0;\"><img src=\""+Constant.CART_IMAGE+"\" alt=\"cart\" width=\"54\" height=\"47\" border=\"0\" style=\"display: block;\" /></td><td valign=\"middle\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 16px;color:#000000;line-height:22px;mso-line-height-rule:exactly;padding:25px 0px 0px 0px ; font-weight: 300; text-decoration: none;\" >Your order confirmation number is <span style=\"color: #0084d6; text-decoration: none; font-weight: 600;\">#");
	htmlBuffer.append(emailData.getOrderNumber());
	htmlBuffer.append("</span>.</td></tr></table></td></tr></table></td></tr><tr><td align=\"center\" valign=\"top\" bgcolor=\"#000000\"><table width=\"516\" style=\"width: 516px; max-width: 516px;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\"><tr><td align=\"left\" valign=\"middle\"  style=\"padding: 16px 0px 0px 0px;\"><img src=\""+Constant.HELLOTECH_LOGO+"\" alt=\"HelloTech\" width=\"156\" height=\"27\" border=\"0\" style=\"display: block;\" /></td></tr><tr><td valign=\"top\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 16px;color:#ffffff;line-height:20px;mso-line-height-rule:exactly;padding: 11px 0px 18px 0px; font-weight: 300; text-decoration: none;\" >Need help installing your new Nest products? HelloTech can help you install your devices and get them up and running.<br/><a target=\"_blank\" href=\""+Constant.HELLOTECH_URL+"\" style=\"color: #0084d6; text-decoration: none;\"><span style=\"color: #0084d6; text-decoration: none;\">Visit hellotech.com/optimumnest to learn more.</span></a></td></tr></table></td></tr></table></td></tr></table></td></tr><!-------------- black background ends here-----------------><tr><td align=\"center\" valign=\"top\" bgcolor=\"#ffffff\" style=\"padding: 0px 0px 0px 0px; border:0px;\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#ffffff\" width=\"563\" style=\"width: 563px; min-width: 563px;\"><tr><td align=\"left\" valign=\"top\" style=\"padding-left: 0px;\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\"><tr><td align=\"left\" valign=\"top\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td align=\"left\" valign=\"top\" style=\"font-family: arial, helvetica, sans-serif;font-size: 12px;color:#000000; padding-bottom: 12px; padding-top: 22px;\">&copy; 2017 Nest Labs, Inc, <a onclick=\"return false;\" style=\"cursor:text; text-decoration:none; color:#000001; border: 0 none; outline: none;\"><span class=\"link\">3400 Hillview Ave., Palo Alto, CA. 94304</span></a>. Optimum, the Optimum family of marks, and Optimum logos are registered trademarks of CSC Holdings, LLC. &copy; 2017 CSC holdings, LLC. </td></tr><tr><td align=\"left\" valign=\"top\" style=\"font-family: arial, helvetica, sans-serif; font-size: 12px; color: #000000; line-height: 15px; mso-line-height-rule:exactly; padding-bottom: 50px;\"><a href=\""+Constant.CUSTOMER_PRIVACY_POLICY+"\" target=\"_blank\" style=\"color:#000000;\"><span style=\" text-decoration:underline;\">Customer Privacy Policy</span></a></td></tr></table></td></tr></table></td></tr><tr><td align=\"left\" valign=\"top\"></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table></div></td></tr></table></body></html>]]>");
	}catch (Exception e) {
		logger.error("Error occured while generating HTML",e);
	}
	System.out.println("htmlBuffer"+htmlBuffer);
	return htmlBuffer;	
	}
	
	
}


