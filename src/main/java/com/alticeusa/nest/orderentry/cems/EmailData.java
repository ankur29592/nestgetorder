package com.alticeusa.nest.orderentry.cems;

import com.alticeusa.nest.orderentry.utils.Constant;

public class EmailData {
	
	    private String recepientAdd ="";
		private String nestCamIndoorQuantity="0";
		private String nestCamOutdoorQuantity="0";
		private String nestThermostatQuantity="0";
		private String nestThermostatEQuantity="0";
		private String nestProtectBattQuantity="0";
		private String nestProtectLnVoltQuantity="0";
		private String googleHomeQuantity="0";
		private String nestCamIQIndoorQuantity= "0";
		private int orderNumber;
		
		public String getRecepientAdd() {
			return recepientAdd;
		}
		public void setRecepientAdd(String recepientAdd) {
			this.recepientAdd = recepientAdd;
		}
	    public int getOrderNumber() {
			return orderNumber;
		}
		public void setOrderNumber(int i) {
			this.orderNumber = i;
		}
		
		private String html = "<![CDATA[<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title>optimum.</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><meta name=\"robots\" content=\"no index\" /><!--[if !mso]><!--><style>@import url('https://fonts.googleapis.com/css?family=Montserrat:300,600');</style><!--<![endif]--><style type=\"text/css\">.ExternalClass * {line-height: 100%;}.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td {line-height: 100%;}.gmailfix { display: none; display: none!important;}a[href^=tel], .nolinkcolor > a { color: inherit; text-decoration: none;}table, td { border-collapse: collapse; padding: 0px; margin: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}sup, td { -webkit-text-size-adjust: none;}/* Yahoo overrides */em { font-style: italic;}strong { font-weight: bold;}a:link { border: none !important; outline: none !important;}.link { color: inherit !important; text-decoration: inherit !important;}a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important;}th { font-weight: normal;}.appleLink, .appleLink a { color: #000000 !important; text-decoration:none !important;}span.appleLink a:link { color: #000000 !important;}span.appleLink a:hover { color: #000000 !important;}span.appleLink a:active { color: #000000 !important;}span.appleLink a:visited { color:#000000 !important;}a[href^=tel], .nolinkcolor a, .ms-font-color-themePrimary, .ms-fcl-tp, .ms-font-color-themePrimary-hover:hover, .ms-font-color-themePrimary-focus:focus, .ms-font-color-themePrimary-before:before, .ms-fcl-tp-h:hover, .ms-fcl-tp-f:focus, .ms-fcl-tp-b:before, .ms-border-color-themePrimary, .ms-bcl-tp, .ms-border-color-themePrimary-hover:hover, .ms-border-color-themePrimary-focus:focus, .ms-border-color-themePrimary-before:before, .ms-bcl-tp-h:hover, .ms-bcl-tp-f:focus, .ms-bcl-tp-b:before { color: inherit !important; text-decoration: none !important;}img { display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}}</style><!--[if mso]><style type=\"text/css\">body, table, td, span {font-family: Arial, Helvetica, sans-serif !important;}</style><! [end if]--> <!--[if mso | ie]><style>.sup {vertical-align: 1px!important; font-size: 90% !important;}</style><![endif]--></head><body style=\"margin:0px; padding:0px; width:100% !important; -ms-text-size-adjust: none;\" bgcolor=\"#fffffe\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" align=\"center\"><tr> <td align=\"center\" valign=\"top\"><div align=\"center\" class=\"desktoponly\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" align=\"center\"><tr><td align=\"center\" valign=\"top\" style=\"min-width:800px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"800\" align=\"center\"><tr><td valign=\"top\" align=\"left\"><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#fffffe\"><tr><td valign=\"top\" align=\"center\"><table width=\"800\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"  style=\"min-width: 800px; width: 800px;\"><tr><td valign=\"top\" align=\"center\" style=\"padding: 0px 0px 40px 0px;\" bgcolor=\"#d6d6d6\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td align=\"center\" valign=\"top\" style=\"padding: 40px 15px 42px 15px;\" bgcolor=\"#000000\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"349\" style=\"min-width: 349px; width: 349px;\"><tr><td width=\"97\" align=\"left\" valign=\"top\"><img src=\""+Constant.NEST_LOGO+"\" alt=\"nest\" width=\"97\" height=\"45\" border=\"0\" style=\"display: block;\" /></td><td align=\"center\" valign=\"top\" style=\"padding: 0 36px;\"><img src=\""+Constant.NEST_HEADER_BAR+"\" alt=\"\" width=\"2\" height=\"45\" border=\"0\" style=\"display: block;\" /></td><td width=\"178\" align=\"left\" valign=\"top\"><a href=\""+Constant.OPTIMUM_LOGO_URL+"\" target=\"_blank\"><img src=\""+Constant.OPTIMUM_LOGO+"\" alt=\"Optimum. | a brand of altice\" width=\"178\" height=\"47\" border=\"0\" style=\"display: block;\" /></a></td></tr></table></td></tr><tr><td align=\"center\" valign=\"top\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"563\" bgcolor=\"#d6d6d6\" align=\"center\"><tr><td valign=\"top\" align=\"center\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 33px;color:#000000;line-height:36px;mso-line-height-rule:exactly;padding:25px 0px 20px 0px ; font-weight: 300; text-decoration: none\">Happiness starts at home.</td></tr><tr><td align=\"center\" valign=\"top\" bgcolor=\"#ffffff\"><table width=\"518\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\"><tr><td valign=\"top\" align=\"left\" style=\"font-family: 'Montserrat', Arial, sans-serif;font-size: 20px;color:#000000;line-height:24px;mso-line-height-rule:exactly;padding:27px 0px 30px 0px ; font-weight: 600; text-decoration: none; border-bottom: 1px solid #000000;\" >Thank you for your order!<br/>You will be receiving the following:</td></tr>";
	    
	    
	  
	  
	    public String getHtml() {
			return html;
		}
		public void setHtml(String html) {
			this.html = html;
		}
		public String getNestCamIndoorQuantity() {
			return nestCamIndoorQuantity;
		}
		public void setNestCamIndoorQuantity(String nestCamIndoorQuantity) {
			this.nestCamIndoorQuantity = nestCamIndoorQuantity;
		}
		public String getNestCamOutdoorQuantity() {
			return nestCamOutdoorQuantity;
		}
		public void setNestCamOutdoorQuantity(String nestCamOutdoorQuantity) {
			this.nestCamOutdoorQuantity = nestCamOutdoorQuantity;
		}
		public String getNestThermostatQuantity() {
			return nestThermostatQuantity;
		}
		public void setNestThermostatQuantity(String nestThermostatQuantity) {
			this.nestThermostatQuantity = nestThermostatQuantity;
		}
		public String getNestThermostatEQuantity() {
			return nestThermostatEQuantity;
		}
		public void setNestThermostatEQuantity(String nestThermostatEQuantity) {
			this.nestThermostatEQuantity = nestThermostatEQuantity;
		}
		public String getNestProtectBattQuantity() {
			return nestProtectBattQuantity;
		}
		public void setNestProtectBattQuantity(String nestProtectBattQuantity) {
			this.nestProtectBattQuantity = nestProtectBattQuantity;
		}
		public String getNestProtectLnVoltQuantity() {
			return nestProtectLnVoltQuantity;
		}
		public void setNestProtectLnVoltQuantity(String nestProtectLnVoltQuantity) {
			this.nestProtectLnVoltQuantity = nestProtectLnVoltQuantity;
		}
		public String getGoogleHomeQuantity() {
			return googleHomeQuantity;
		}
		public void setGoogleHomeQuantity(String googleHomeQuantity) {
			this.googleHomeQuantity = googleHomeQuantity;
		}
		public String getNestCamIQIndoorQuantity() {
			return nestCamIQIndoorQuantity;
		}
		public void setNestCamIQIndoorQuantity(String nestCamIQIndoorQuantity) {
			this.nestCamIQIndoorQuantity = nestCamIQIndoorQuantity;
		}
		
}
