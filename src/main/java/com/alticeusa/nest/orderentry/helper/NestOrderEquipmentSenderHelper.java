package com.alticeusa.nest.orderentry.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.common.auth.NestEquipmentClient;
import com.alticeusa.nest.common.base.GenericResponse;
import com.alticeusa.nest.orderentry.dao.NestOrderServiceDao;
import com.alticeusa.nest.orderentry.data.NestEquipInput;
import com.alticeusa.nest.orderentry.data.NestOrderEquipmentPojo;
import com.alticeusa.nest.orderentry.metrics.logging.LogFormatter;
import com.alticeusa.nest.orderentry.utils.Constant;
import com.alticeusa.nest.orderentry.utils.Utils;

public class NestOrderEquipmentSenderHelper {

	private static final Logger logger = LogManager.getLogger(NestOrderEquipmentSenderHelper.class);
	
	public static void sendToNest(NestOrderEquipmentPojo nestOrderEquipmentPojo, String recieveMsg) {
		logger.info(">>>>> In Helper Equipment messgae sendToNest() for message: "+recieveMsg);
		int sendRetry = Constant.NEST_ERROR_THRESHOLD_VALUE;
		String nestInput = null;
		try {
			NestEquipInput nestEquipInput = new NestEquipInput(nestOrderEquipmentPojo.getOrders());
			nestInput = Utils.convertJavaToJson(nestEquipInput);
			logger.info(">>> Equipment sendToNest() ::: request = {}", LogFormatter.produceJSON(nestEquipInput, Constant.PRETTY_PRINT));
			GenericResponse nestResponse = sendMessage(nestInput,nestOrderEquipmentPojo,sendRetry,recieveMsg);
			if(nestResponse!=null) {
				updateNestEquipStatusToDb(nestOrderEquipmentPojo,nestResponse);
			}
		} catch (Exception e) {
			logger.error("Exception occured while sending Equipment data to nest for orderId: " + nestOrderEquipmentPojo.getOrderId(), e);
			NestOrderServiceDao nestOrderServiceDao =  new NestOrderServiceDao();
			nestOrderServiceDao.updateOrderEquipmentError(nestOrderEquipmentPojo.getOrderId(), 1, "Error while sending Equipment data to nest",nestInput,Constant.ERROR,Constant.NEST_DEFAULT_ERROR_STATUS);
		}
		logger.info("<<<<< out Helper Equipment messgae sendToNest() for message: "+recieveMsg);
	}
	
	private static GenericResponse sendMessage(String nestInput, NestOrderEquipmentPojo nestOrderEquipmentPojo,int sendRetry,String recieveMsg) {
		GenericResponse nestResponse = null;
		try {
			logger.debug("Sending Bulk Equipment request to nest for orderId: " + nestOrderEquipmentPojo.getOrderId());
			nestResponse = NestEquipmentClient.createNestOrder(nestInput, nestOrderEquipmentPojo.getOrderId());
			logger.debug("Bulk Equipment request sent to nest for orderId: " + nestOrderEquipmentPojo.getOrderId());
		} catch (Exception e) {
			logger.error("Exception occured while calling NestEquipmentClient.createNestOrder() for bulk equipment orderId" + nestOrderEquipmentPojo.getOrderId(), e);
			if(sendRetry > 0){
				logger.info("Retrying to sent bulk request to nest");
				sendMessage(nestInput,nestOrderEquipmentPojo,sendRetry-1,recieveMsg);
			} else {
				logger.debug("Not able to connect nest, calling dao updateOrderEquipmentError() for orderId: " + nestOrderEquipmentPojo.getOrderId());
				NestOrderServiceDao nestOrderServiceDao = new NestOrderServiceDao();
				nestOrderServiceDao.updateOrderEquipmentError(nestOrderEquipmentPojo.getOrderId(), Constant.NEST_ERROR_THRESHOLD_VALUE,"Error while try to hit the nest URL for Equipment",recieveMsg,Constant.ERROR,Constant.NEST_DEFAULT_ERROR_STATUS);
			}
		}
		return nestResponse;
	}
	
	private static void updateNestEquipStatusToDb(NestOrderEquipmentPojo nestOrderEquipmentPojo,GenericResponse nestResponse) {
		logger.info(">>>>> In Helper Equipment  updateNestEquipStatusToDb for orderId: "+nestOrderEquipmentPojo.getOrderId());
		NestOrderServiceDao nestOrderServiceDao =  new NestOrderServiceDao();
		NestOrderEquipmentRecieverHelper nestOrderEquipmentRecieverHelper;
		int[] orderCount = new int[2];
	
		try {
			if(nestResponse.getResponseCode() == 200) {
				logger.debug("Recieve 200 http response for orderId: " + nestOrderEquipmentPojo.getOrderId() );
				nestOrderEquipmentRecieverHelper = (NestOrderEquipmentRecieverHelper) Utils.convertJsonToNestOrderEquipmentRecieverHelper(nestResponse.getResponseBody());
				logger.info(">>> Equipment updateNestEquipStatusToDb() ::: response = {}", LogFormatter.produceJSON(nestOrderEquipmentRecieverHelper, Constant.PRETTY_PRINT));
				logger.debug("calling updateOrderEquipmentInfo() to update nest status for orderId: " + nestOrderEquipmentPojo.getOrderId());
				nestOrderServiceDao.updateOrderEquipmentInfo(nestOrderEquipmentRecieverHelper,nestOrderEquipmentPojo.getOrderId());
				logger.debug("calling updateOrderSummaryInfo() to update nest status for orderId: " + nestOrderEquipmentPojo.getOrderId());
				nestOrderServiceDao.updateOrderSummaryInfo(nestOrderEquipmentRecieverHelper,nestOrderEquipmentPojo.getOrderId());
				/*try{
					orderCount = nestOrderServiceDao.checkSubscriptions(nestOrderEquipmentPojo.getOrderId());
					int equipCount=orderCount[0];
					int subCount = orderCount[1];
					logger.info("Subscription Count in EquipmentHelper: "+subCount);
					logger.info("Equipment Count in EquipmentHelper: "+equipCount);
					if(equipCount>=1 && subCount==0){
						logger.info("Sending mail from EquipmentHelper");
						nestOrderServiceDao.sendShippingMail(nestOrderEquipmentPojo.getOrderId());
					}
				}catch (Exception e) {
					logger.error("Error while sending mail to Cems in NestOrderEquipmentSenderHelper class" ,e);
				}*/
				} else {
				logger.debug("Recieve fail http response for orderId: " + nestOrderEquipmentPojo.getOrderId() );
				if(nestResponse.getResponseBody()!=null) {
					nestOrderEquipmentRecieverHelper = (NestOrderEquipmentRecieverHelper) Utils.convertJsonToNestOrderEquipmentRecieverHelper(nestResponse.getResponseBody());
					logger.info(">>> Equipment updateNestEquipStatusToDb() ::: Error response = {}", LogFormatter.produceJSON(nestOrderEquipmentRecieverHelper, Constant.PRETTY_PRINT));
					logger.debug("calling updateOrderEquipmentError() to update nest error status for orderId: " + nestOrderEquipmentPojo.getOrderId());
					nestOrderServiceDao.updateOrderEquipmentError(nestOrderEquipmentPojo.getOrderId(), 1,nestOrderEquipmentRecieverHelper.getValidationResult().getMessage(),nestResponse.getResponseBody(),Constant.ERROR,nestOrderEquipmentRecieverHelper.getValidationResult().getStatus());
				} else {
					logger.debug("calling updateOrderEquipmentError() to update nest error status for orderId: " + nestOrderEquipmentPojo.getOrderId());
					nestOrderServiceDao.updateOrderEquipmentError(nestOrderEquipmentPojo.getOrderId(), 1,"Unreachable to Nest",nestResponse.getResponseBody(),Constant.ERROR,Constant.NEST_DEFAULT_ERROR_STATUS);
				}
			}
		} catch (Exception e) {
			logger.error("Exception occured while inserting NestResponse into DB for orderId: " + nestOrderEquipmentPojo.getOrderId(), e);
			nestOrderServiceDao.updateOrderEquipmentError(nestOrderEquipmentPojo.getOrderId(), 1,"Error while inserting NestResponse into DB",nestResponse.getResponseBody(),Constant.ERROR,Constant.NEST_DEFAULT_ERROR_STATUS);
		}
		logger.info("<<<<< Out Helper Equipment  updateNestEquipStatusToDb for orderId: "+nestOrderEquipmentPojo.getOrderId());
	}
}
