package com.alticeusa.nest.orderentry.helper;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.alticeusa.nest.orderentry.data.Item;
import com.alticeusa.nest.orderentry.data.ValidationResult;

public class NestOrderEquipmentRecieverHelper {

	@JsonProperty("limit")
    private Integer limit;
    @JsonProperty("offset")
    private Integer offset;
    @JsonProperty("href")
    private String href;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonProperty("fields")
    private List<String> fields = null;
    @JsonProperty("total_records")
    private Integer totalRecords;
    @JsonProperty("validation_result")
    private ValidationResult validationResult;
    
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	public List<String> getFields() {
		return fields;
	}
	public void setFields(List<String> fields) {
		this.fields = fields;
	}
	public Integer getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}
	public ValidationResult getValidationResult() {
		return validationResult;
	}
	public void setValidationResult(ValidationResult validationResult) {
		this.validationResult = validationResult;
	}
}
