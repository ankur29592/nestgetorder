package com.alticeusa.nest.orderentry.helper;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.common.auth.NestSubscriptionClient;
import com.alticeusa.nest.common.base.GenericResponse;
import com.alticeusa.nest.orderentry.dao.NestOrderServiceDao;
import com.alticeusa.nest.orderentry.data.NestOrderSubscriptionPojo;
import com.alticeusa.nest.orderentry.metrics.logging.LogFormatter;
import com.alticeusa.nest.orderentry.utils.Constant;
import com.alticeusa.nest.orderentry.utils.Utils;

public class NestOrderSubscriptionSenderHelper {
	//public static HashMap<String, String> orderIdMap=new HashMap<String, String>();
	private static final Logger logger = LogManager.getLogger(NestOrderSubscriptionSenderHelper.class);
	
	public static void sendToNest(NestOrderSubscriptionPojo nestOrderSubscriptionPojo,String nestInput) {
		logger.info(">>>>> In Helper Subscription messgae sendToNest() for message: "+nestInput);
		int sendRetry = Constant.NEST_ERROR_THRESHOLD_VALUE;
		try {
			NestSubscriptionClient noc=new NestSubscriptionClient();
			logger.info(">>> Subscription sendToNest() ::: request = {}", LogFormatter.produceJSON(nestOrderSubscriptionPojo, Constant.PRETTY_PRINT));
			GenericResponse nestResponse = sendMessage(nestInput,nestOrderSubscriptionPojo,noc,sendRetry);
			logger.debug("Nest Respone for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId() + " nest response " + nestResponse);
			if(nestResponse!=null) {
				updateNestSubscrbStatusToDb(nestResponse,nestOrderSubscriptionPojo,nestInput);
			}
		} catch (Exception e) {
			logger.error("Exception occured while sending subscription data to nest for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId(), e);
			NestOrderServiceDao nestOrderServiceDao = new NestOrderServiceDao();
			nestOrderServiceDao.updateOrderSubscriptionError(nestOrderSubscriptionPojo.getPartnerCustomerId(), nestOrderSubscriptionPojo.getPartnerSubscriptionId(), Constant.ERROR, 1, "Error while try to connect NEST for subscription", nestInput);
		}
		logger.info("<<<<< out Helper Subscription messgae sendToNest() for message: "+nestInput);
	}
	
	private static GenericResponse sendMessage(String nestInput,NestOrderSubscriptionPojo nestOrderSubscriptionPojo,NestSubscriptionClient noc,int sendRetry) {
		GenericResponse nestResponse = null;
		try {
			logger.debug("Sending subscription request to nest for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId());
			nestResponse = noc.createNestPartnerSubscription(nestInput);
			logger.debug("Subscription request sent to nest for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId());
		} catch (Exception e) {
			logger.error("Exception occured while calling NestSubscriptionClient.createNestPartnerSubscription() for subscription orderId" +nestOrderSubscriptionPojo.getPartnerCustomerId(), e);
			if(sendRetry > 0){
				 logger.info("Retrying to subscription request to nest");
				 sendMessage(nestInput,nestOrderSubscriptionPojo,noc,sendRetry-1);
			}else {
				logger.debug("Not able to connect nest, calling dao updateOrderSubscriptionError() for orderId: " +nestOrderSubscriptionPojo.getPartnerCustomerId());
				NestOrderServiceDao nestOrderServiceDao = new NestOrderServiceDao();
				nestOrderServiceDao.updateOrderSubscriptionError(nestOrderSubscriptionPojo.getPartnerCustomerId(), nestOrderSubscriptionPojo.getPartnerSubscriptionId(), Constant.ERROR, Constant.NEST_ERROR_THRESHOLD_VALUE, "Error while trying to hit nest URL", nestInput);
				
			}
		}
		return nestResponse;
	}
	private static void updateNestSubscrbStatusToDb(GenericResponse nestResponse,NestOrderSubscriptionPojo nestOrderSubscriptionPojo,String nestInput) {
		
		logger.info(">>>>> In Helper Subscription  updateNestSubscrbStatusToDb for orderId: "+nestOrderSubscriptionPojo.getPartnerCustomerId());
		
		NestOrderServiceDao nestOrderServiceDao = new NestOrderServiceDao();

		try {
			if (nestResponse.getResponseCode() == 200) {
				logger.debug("Recieve 200 http response for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId() );
				NestOrderSubscriptionRecieverSuccess nestOrderSubscriptionRecieverSuccess;
				nestOrderSubscriptionRecieverSuccess = (NestOrderSubscriptionRecieverSuccess) Utils.convertJsonToNestOrderSubscriptionRecieverSuccess(nestResponse.getResponseBody());
				logger.info(">>> Subscription updateNestSubscrbStatusToDb() ::: response = {}", LogFormatter.produceJSON(nestOrderSubscriptionRecieverSuccess, Constant.PRETTY_PRINT));
				
				if ("ACTIVE".equalsIgnoreCase(nestOrderSubscriptionRecieverSuccess.getStatus())) {
					logger.debug("calling updateOrderSubscriptionInfo() to update nest status for orderId: " +  nestOrderSubscriptionPojo.getPartnerCustomerId());
					nestOrderServiceDao.updateOrderSubscriptionInfo(nestOrderSubscriptionRecieverSuccess);
					/*int orderID = Integer.parseInt(nestOrderSubscriptionPojo.getPartnerCustomerId());
					
					try{
							logger.info("Sending mail from SubscriptionHelper");
							
							if(!orderIdMap.containsKey(nestOrderSubscriptionPojo.getPartnerCustomerId())) {
								logger.debug("Inserting "+orderID+" in the hashmap");
								orderIdMap.put(nestOrderSubscriptionPojo.getPartnerCustomerId(), nestOrderSubscriptionPojo.getPartnerCustomerId());
								logger.debug("Going to sleep for 1 min");
								Thread.sleep(Constant.WAIT_TIME);
								nestOrderServiceDao.sendShippingMail(orderID);
							}

							    
					}catch (Exception e) {
						logger.error("Error while sending mail to Cems in NestOrderEquipmentSenderHelper class" ,e);
						orderIdMap.remove(nestOrderSubscriptionPojo.getPartnerCustomerId());
					}
					if(orderIdMap.containsKey(nestOrderSubscriptionPojo.getPartnerCustomerId())) {
						nestOrderServiceDao.setActiveStatus(orderID);
					}*/
				} else {
					nestOrderServiceDao.updateOrderSubscriptionStatus(nestOrderSubscriptionRecieverSuccess);
				}
			} else {
				logger.debug("Recieve fail http response for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId() );
				NestOrderSubscriptionRecieverError nestOrderSubscriptionRecieverError = (NestOrderSubscriptionRecieverError) Utils.convertJsonToNestOrderSubscriptionRecieverError(nestResponse.getResponseBody());
				logger.info(">>> Subscription updateNestSubscrbStatusToDb() ::: Error response = {}", LogFormatter.produceJSON(nestOrderSubscriptionRecieverError, Constant.PRETTY_PRINT));
				logger.debug("calling updateOrderEquipmentError() to update nest error status for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId());
				nestOrderServiceDao.updateOrderNestSubscriptionError(nestOrderSubscriptionRecieverError,nestOrderSubscriptionPojo);
			}
		} catch (Exception e) {
			logger.error("Exception occured while inserting NestResponse into DB for orderId: " + nestOrderSubscriptionPojo.getPartnerCustomerId(), e);
			nestOrderServiceDao.updateOrderSubscriptionError(nestOrderSubscriptionPojo.getPartnerCustomerId(), nestOrderSubscriptionPojo.getPartnerSubscriptionId(), Constant.ERROR, 1, "Error while trying to update Nest status", nestInput);
		}
		logger.info("<<<<< Out Helper Subscription  updateNestSubscrbStatusToDb for orderId: "+nestOrderSubscriptionPojo.getPartnerCustomerId());
	}
}
